({
    init: function(cmp,event){
        var now = new Date();
        var sixMonthsBack = new Date();
        sixMonthsBack.setMonth(now.getMonth() - 6);
        cmp.find("comparisonStartDate").set("v.value",sixMonthsBack);
        cmp.find("comparisonEndDate").set("v.value",now);
        cmp.find("targetStartDate").set("v.value",sixMonthsBack);
        cmp.find("targetEndDate").set("v.value",now);
    },
    
    formatDate: function(date){
        var returnedDate = $A.localizationService.formatDate(date);
        console.log(returnedDate);
      return returnedDate; 
    },
    
    formatDate1: function(date)
    {
      var month = date.getMonth();  
      var day = date.getDate();
      var year = date.getFullYear();
      return  this.prefix((month+1)) + "/" + this.prefix(day) + "/" + this.prefix(year);
    },
    prefix: function(num)
    {
        if(num < 10)
        {
            return "0" + num;
        }else
        {
            return num;
        }
    },    
    compareBtnClicked: function(cmp,event){
        
        //DISABLE UI BUTTON
        var btn = event.getSource();
        btn.set("v.disabled",true);
        
        var audience = cmp.get("v.audience");
        var targetStartDate = cmp.find("targetStartDate").get("v.value");
        var targetEndDate = cmp.find("targetEndDate").get("v.value");
        var comparisonStartDate = cmp.find("comparisonStartDate").get("v.value");
        var comparisonEndDate = cmp.find("comparisonEndDate").get("v.value");
        var params = {
                      targetAudienceId: audience.id,
                      startDate: targetStartDate,
                      endDate: targetEndDate,
                      comparisonStartDate: comparisonStartDate,
                      comparisonEndDate: comparisonEndDate,
                      button: btn
                    };
        
        //CREATE AND FIRE AUDIENCE EVENT
        var comparisonEvent = $A.get("e.c:AudienceComparisonEvent");
        comparisonEvent.setParams(params);
        comparisonEvent.fire();
    },
    
    backToAudiencesLinkClicked: function(cmp, event){
        //CREATE AND FIRE AUDIENCE EVENT
        var audienceEvent = $A.get("e.c:BackToAudiencesEvent");
        audienceEvent.setParams({});
        audienceEvent.fire();
    },
    
	useSameDatesCheckboxClicked: function(cmp,event){
        var checkCmp = cmp.find("sameDatesCheckbox");
       
        var sameDates = checkCmp.get("v.checked");
        cmp.set("v.useSameDates",sameDates);
        
        if(sameDates)
        {
            $A.util.addClass(cmp.find("targetStartDateDiv"), "hidden");
            $A.util.addClass(cmp.find("targetEndDateDiv"), "hidden");
        }else
        {
            $A.util.removeClass(cmp.find("targetStartDateDiv"), "hidden");
            $A.util.removeClass(cmp.find("targetEndDateDiv"), "hidden");
        }
        
    }
})